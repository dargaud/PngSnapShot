// Warning, will compile only in CVI 5.5 + Patch 1

#include <Windows.h>
#include <Winuser.h>

#include <userint.h>
#include <ansi_c.h>
#include <utility.h>
#include <cvirte.h>    /* Needed if linking in external compiler; harmless otherwise */

#include "wintools.h"
#include "toolbox.h"
#include "StrfTime.h"

#include "PngSnapShot.h"

static int	Pnl=0, 
			TimerPeriod=30,		// seconds
			ArchivePerDay=4,	// 
			FullSnap=TRUE;
static char DestPngFile[MAX_PATHNAME_LEN]="SnapShot.png", 
			ArchiveDir[MAX_PATHNAME_LEN]="";

#define LEN 255
static char TimeFormat[LEN]="%Y%m%d_%H%M";

///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	unsigned long _PPCM;
	int Len;
	if (InitCVIRTE (0, argv, 0) == 0)	/* Needed if linking in external compiler; harmless otherwise */
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "PngSnapShot.uir", PNL)) < 0) {
		MessagePopup("Error", "Could not load panel from file PngSnapShot.uir");
		return -1;
	}
	DisplayPanel (Pnl);

	SetSleepPolicy (VAL_SLEEP_MORE);
	WT_RegReadString(WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "DestPngFile", DestPngFile, sizeof(DestPngFile), &Len);
	WT_RegReadString(WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "ArchiveDir", ArchiveDir, sizeof(ArchiveDir), &Len);
	if (strlen(ArchiveDir)>0 && ArchiveDir[strlen(ArchiveDir)-1]!='\\') {
		ArchiveDir[strlen(ArchiveDir)+1]!='\0';
		ArchiveDir[strlen(ArchiveDir)]='\\';
	}
	
	WT_RegReadULong (WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "TimerPeriod", (unsigned long*)&TimerPeriod, 0);
	WT_RegReadULong (WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "ArchivePerDay", (unsigned long*)&ArchivePerDay, 0);
	WT_RegReadULong (WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "FullSnap", (unsigned long*)&FullSnap, 0);
	SetCtrlVal(Pnl, PNL_PERIOD, TimerPeriod);
	SetCtrlVal(Pnl, PNL_ARCHIVE_PER_DAY, ArchivePerDay);
	SetCtrlVal(Pnl, PNL_FULLSNAP, FullSnap);
	SetCtrlAttribute (Pnl, PNL_TIMER, ATTR_INTERVAL, (double)TimerPeriod);
	
	RunUserInterface ();
	return 0;
}



void GetAndSaveImage(void) {
	int Error, BmpID=0, Available;
	int NewBmp, Status, Height, Width;
	int PixDepth, RowB, Grp;
	unsigned char *Mask=NULL, *Bits=NULL, *NewMask=NULL, *Array=NULL;
	int *ColorTable=NULL;
	static int LastGrp=-1, LastDay=-1;
	int Year, Day, Month, Hour, Min, Sec;
			
////////////	Emulates the [PtrScr] key
//	keybd_event(VK_PRINT, 0x45, KEYEVENTF_EXTENDEDKEY, 0);
//	keybd_event(VK_PRINT, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
	
//	keybd_event(VK_SNAPSHOT, 0, 0, 0);
	keybd_event(VK_SNAPSHOT, (BYTE)FullSnap, 0, 0);	// 1 for full screen, 0 for top Window

//	keybd_event(VK_SNAPSHOT, 1, KEYEVENTF_KEYUP, 0);
	
	Delay(2.);		// Small delay

////////////	Check clipboard

	Error=ClipboardGetBitmap (&BmpID, &Available);
	if (Error!=0) {
		SuspendTimerCallbacks ();
		MessagePopup("Error !", GetUILErrorString(Error));
		ResumeTimerCallbacks ();
		return;
	} else if (!Available) {
		SuspendTimerCallbacks ();
		MessagePopup ("Warning !", "The clipboard does not seem to contain a valid image.");
		ResumeTimerCallbacks ();
		return;
	}


////////////	Allocation and copy to buffer
	if (ColorTable!=NULL) free(ColorTable); ColorTable=NULL;
	if (Bits!=NULL) free(Bits); Bits=NULL;
	if (Mask!=NULL) free(Mask); Mask=NULL;

//	if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); QuitUserInterface(1); }
	Status = AllocBitmapData (BmpID, &ColorTable, &Bits, &Mask);
	if (Status!=0) { 
		MessagePopup("Error!",GetUILErrorString (Status)); 
		return; 
	}
	if (ColorTable!=NULL) {	
		SuspendTimerCallbacks ();
		MessagePopup ("Warning !", "This program operates only on\n16 million color files.\nExiting.");
		QuitUserInterface(1); 
	}
	if (Mask!=NULL) {	
		SuspendTimerCallbacks ();
		MessagePopup ("Warning !", "This program operates only on\nnon masked files.\nExiting.");
		QuitUserInterface(1); 
	}
	Status = GetBitmapData (BmpID, &RowB, &PixDepth, &Width, &Height, ColorTable, Bits, Mask);
	if (Status!=0) { MessagePopup("Error!",GetUILErrorString (Status)); QuitUserInterface(1); }

//	SetCtrlVal(Pnl, PNL_WIDTH, Width);
//	SetCtrlVal(Pnl, PNL_HEIGHT, Height);

	//if (Bitmap==0) NewBitmapEx (RowB, PixDepth, Width, Height, NULL, Bits, NULL, NULL, &Bitmap)
	//SetBitmapDataEx (Bitmap, RomB, PixDepth, NULL, Bits, NULL, NULL);
	Error=SaveBitmapToPNGFile (BmpID, "$temp$.png");
	if (Error) {
		char Str[1024];
		sprintf(Str, "Error when saving temporary file: %s", GetUILErrorString (Error));
		MessagePopup("Error", Str);
	}
	Error=CopyFile ("$temp$.png", DestPngFile);
	if (Error) {
		char Str[1024];
		sprintf(Str, "Error when copying to PNG file: %s", strerror(errno==0 ? Error : errno));
		MessagePopup("Error", Str);
	}

	if (ColorTable!=NULL) free(ColorTable); ColorTable=NULL;
	if (Bits!=NULL) free(Bits); Bits=NULL;
	if (Mask!=NULL) free(Mask); Mask=NULL;
	DiscardBitmap(BmpID); BmpID=0;

////////	Archive copy of file
	GetSystemDate(&Month, &Day, &Year);
	GetSystemTime(&Hour,  &Min, &Sec);

	Grp=(Hour*60+Min)*ArchivePerDay/(24*60);
	if ((Grp!=LastGrp || (ArchivePerDay==1 && LastDay!=Day)) && ArchivePerDay!=0) {
		char File[MAX_FILENAME_LEN], ArchiveDest[MAX_PATHNAME_LEN], *Pos, TimeStr[LEN];
		time_t Time=time(NULL);
		struct tm* TM=localtime(&Time);
   
		SplitPath (DestPngFile, NULL, NULL, File);
		strcpy(ArchiveDest, ArchiveDir);
		strcat(ArchiveDest, File);
		Pos = strrchr (ArchiveDest, '.');
		StrfTime (TimeStr, LEN, TimeFormat, TM);
		strcpy(Pos, TimeStr);
		strcat(Pos, ".png");
		
		CopyFile (DestPngFile, ArchiveDest);
	}
	LastGrp=Grp;
	LastDay=Day;
}



int CVICALLBACK CB_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
	}
	return 0;
}



int CVICALLBACK CB_Snapshot (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Height, Width;	
	switch (event) {
		case EVENT_TIMER_TICK:
			GetAndSaveImage();
//			CVI 5.5 bug, OK with patch
//			GetPanelAttribute (Pnl, ATTR_WIDTH, &Width);
//			GetPanelAttribute (Pnl, ATTR_HEIGHT, &Height);
//			SetPanelSize (Pnl, Height, Width%2?Width+1:Width-1);
			break;
	}
	return 0;
}			
			


int CVICALLBACK CB_PasteClipboard (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Available, Error, BmpID;
	switch (event) {
		case EVENT_COMMIT:
			GetAndSaveImage();
			break;
	}
	return 0;
}

int CVICALLBACK CBP_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int Height, Width;
	switch (event) {
		case EVENT_PANEL_SIZE:
//			GetAndSaveImage();
			GetPanelAttribute (Pnl, ATTR_WIDTH, &Width);
			GetPanelAttribute (Pnl, ATTR_HEIGHT, &Height);
			SetPanelSize (Pnl, Max(Height, 100), 220);
			break;
		case EVENT_PANEL_MOVE:

			break;
	}
	return 0;
}

int CVICALLBACK CB_Period (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_PERIOD, &TimerPeriod);
			SetCtrlAttribute (Pnl, PNL_TIMER, ATTR_INTERVAL, (double)TimerPeriod);
			WT_RegWriteULong (WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "TimerPeriod", TimerPeriod, 0);
			break;
	}
	return 0;
}

int CVICALLBACK CB_SelectDestFile(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Ret, Pos;
	char Drive[MAX_DRIVENAME_LEN], Dir[MAX_DIRNAME_LEN], Folder[MAX_PATHNAME_LEN];
	switch (event) {
		case EVENT_COMMIT:
			SplitPath (DestPngFile, Drive, Dir, NULL);
			strcpy(Folder, Drive);
			strcat(Folder, Dir);
		
			Ret = FileSelectPopup (Folder, "*.png", "*.png",
								   "Select destination PNG file", VAL_SAVE_BUTTON,
								   0, 1, 1, 1, DestPngFile);
			if (Ret==VAL_EXISTING_FILE_SELECTED || Ret==VAL_NEW_FILE_SELECTED) {
	//			SetCtrlVal(PnlS2F, PNL_S2F_SPACE_LEFT, AvailableDiskSize(GetInstFileDisk()));
				WT_RegWriteString(WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "DestPngFile", DestPngFile);
			}

			break;
	}
	return 0;
}

int CVICALLBACK CB_FullSnap (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_FULLSNAP, &FullSnap);
			WT_RegWriteULong(WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "FullSnap", FullSnap, 0);
			break;
	}
	return 0;
}

int CVICALLBACK CB_Help (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			MessagePopup("Help for PngSnapShot v1.02", 
						"This small utility periodically performs a snapshot of the screen and saves it to a PNG file."
						"\nYou can use it to monitor a real-time program through a webserver for instance."
						
						"\n\nPNG files are compressed graphic files which can be B&W, greyscale, paletted, 16M colors..."
						"\nThe compression is lossless (like ZIP) and they can be used on the web like JPG or GIF."
						"\nThe files created here are 16M colors, without alpha channel."
						
						"\n\nProgram options are:"
						"\n- Update period for performing the snapshot (in seconds)"
						"\n- File name and location"
						"\n- Full screen snapshot or only active window"
						"\n- Archival directory where to save a copy of the PNG file with a timestamp"
						"\n  This directory is typically configured with directory browse access on a server"
						"\n- Numbers of Archives per day to keep. If set to 0, no archives are saved."
						"\n- Compression level: 1 (fast and big) to 9 (slow and small)"
						"\n- Pix/cm resolution. 28pix/cm is equivalent to 72dpi"
						"\n- Optional text information to save in the PNG file"
						"\n- TimeFormat appended to the archived filename (right-click on it for help"
						
						"\n\nNOTE: the program uses the clipboard as a temporary holder for the image,"
						"\nyou should not use cut and paste operations while this program is running !!!"
						
						"\n\nCopyright 2000 Guillaume Dargaud for CNR/IFA, free use and distribution."
						"\nTutorial and last version download at http://www.gdargaud.net/Hack/PngSnapShot.html"
						"\nPNG code based on LIBPNG public library http://www.cdrom.com/pub/png/"
						"\nLIBPNG uses the ZLIB library http://www.cdrom.com/pub/infozip/zlib/zlib.html");
			break;
	}
	return 0;
}

int CVICALLBACK CB_ArchivePerDay (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &ArchivePerDay);
			WT_RegWriteULong (WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "ArchivePerDay", ArchivePerDay, 0);
			break;
	}
	return 0;
}

int CVICALLBACK CB_SelectArchiveDir (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Ret;
	switch (event) {
		case EVENT_COMMIT:
			Ret = DirSelectPopup (ArchiveDir, "Select Archive Directory", 1, 1, ArchiveDir);
			if (Ret==VAL_DIRECTORY_SELECTED) {
	//			SetCtrlVal(PnlS2F, PNL_S2F_SPACE_LEFT, AvailableDiskSize(GetInstFileDisk()));
				if (strlen(ArchiveDir)>0 && ArchiveDir[strlen(ArchiveDir)-1]!='\\') {
					ArchiveDir[strlen(ArchiveDir)+1]!='\0';
					ArchiveDir[strlen(ArchiveDir)]!='\\';
				}
				WT_RegWriteString(WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "ArchiveDir", ArchiveDir);
			}
			break;
	}
	return 0;
}

int CVICALLBACK CB_TimeFormat (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, TimeFormat);
			WT_RegWriteString(WT_KEY_HKCU, "SOFTWARE\\CNR\\PngSnapShot", "TimeFormat", TimeFormat);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Time Format Help", 
				"Normal ANSI C formatting codes:\n"
				"    %a  abbreviated weekday name    \"Mon\"...\n"
				"    %A  full weekday name           \"Monday\"...\n"
				"    %b  abbreviated month name      \"Jan\"...\n"
				"    %B  full month name             \"January\"...\n"
				"    %c  locale-specific date and time\n"
				"    %d  day of the month as integer 01-31\n"
				"    %H  hour (24-hour clock)        00-23\n"
				"    %I  hour (12-hour clock)        01-12\n"
				"    %j  day of the year as integer  001-366\n"
				"    %m  month as integer            01-12\n"
				"    %M  minute as integer           00-59\n"
				"    %p  locale AM/PM designation\n"
				"    %S  second as integer           00-61 (allow for up to 2 leap seconds)\n"
				"    %U  week number of the year     00-53 (week number 1 has 1st sunday)\n"
				"    %w  weekday as integer          (0-6, sunday=0)\n"
				"    %W  week number of the year     00-53 (week number 1 has 1st monday)\n"
				"    %x  locale specific date\n"
				"    %X  locale specific time\n"
				"    %y  year without century        00-99\n"
				"    %Y  year with century           1900...\n"
				"    %z  time zone name\n"
				"    %%  a single %%\n");
			MessagePopup("Time Format Help", 
				"Additional formatting codes\n"
				"(example given for Fri Dec 31 23:59:59 1999)\n"
				"\n"
				"    %n  Decimal minutes,           59.983333\n"
				"    %h  Decimal hour,              23.999722\n"
				"    %D  Fraction of day,           0.999988\n"
				"    %J  Decimal julian day,        364.999988\n"
				"    %q  fractional year,           0.9999999682191 (warning, non linear)\n"
				"    %Q  Decimal year,              1999.9999999682191 (warning, non linear)\n"
				"    %o  Letter month, lowercase    l\n"
				"    %O  Letter month, uppercase    L\n"
				"    %1  hours since start of year  8759\n"
				"    %2  hours since start of month 743\n"
				"    %3  min since start of year    525599\n"
				"    %4  min since start of month   44639\n"
				"    %5  min since start of day     1439\n"
				"    %6  sec since start of year    31535999\n"
				"    %7  sec since start of month   2678399\n"
				"    %8  sec since start of day     86399\n"
				"    %9  sec since start of hour    3599\n"
				"    %t  time_t internal (compiler/system specific, number of seconds since 1900 or 1970)  3155666399\n");
			break;
	}
	return 0;
}
