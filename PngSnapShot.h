/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PNL                              1       /* callback function: CBP_Panel */
#define  PNL_PERIOD                       2       /* control type: numeric, callback function: CB_Period */
#define  PNL_SELECT_DEST_FILE             3       /* control type: pictButton, callback function: CB_SelectDestFile */
#define  PNL_FULLSNAP                     4       /* control type: binary, callback function: CB_FullSnap */
#define  PNL_ARCHIVE_PER_DAY              5       /* control type: numeric, callback function: CB_ArchivePerDay */
#define  PNL_SELECT_ARCHIVE_DIR           6       /* control type: pictButton, callback function: CB_SelectArchiveDir */
#define  PNL_PASTE                        7       /* control type: command, callback function: CB_PasteClipboard */
#define  PNL_HELP                         8       /* control type: command, callback function: CB_Help */
#define  PNL_QUIT                         9       /* control type: command, callback function: CB_Quit */
#define  PNL_TIME_FORMAT                  10      /* control type: string, callback function: CB_TimeFormat */
#define  PNL_TIMER                        11      /* control type: timer, callback function: CB_Snapshot */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK CB_ArchivePerDay(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_FullSnap(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Help(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_PasteClipboard(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Period(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_SelectArchiveDir(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_SelectDestFile(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_Snapshot(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_TimeFormat(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CBP_Panel(int panel, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
