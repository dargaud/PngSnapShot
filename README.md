PROGRAM:   PngSnapShot.exe  

VERSION:   1.05  

COPYRIGHT: 2000-2002 Guillaume Dargaud - Freeware - GPL  

PURPOSE:   Periodically saves the screen to a PNG file.  

HELP:      [Help] button on the user interface.  

TUTORIAL:  http://www.gdargaud.net/Hack/PngSnapShot.html  

KNOWN BUG: The program uses the clipboard to hold temporarily the image;  
            so you should not use any cut/paste while it is running !  
            (this program was designed to run on acquisition PCs without users)  

Old tool for taking regular screenshots of an application or the entire screen. Written in LabWindows/CVI  
This is a mostly obsolete program.
